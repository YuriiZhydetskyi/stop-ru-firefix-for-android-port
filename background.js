let LIMIT_UPPER = 3500;
let LIMIT_LOWER = 3000;

let userId;
let userProfileCache = {};  // from DB

try {

  function isEmpty(obj) {
    if (typeof obj == 'undefined') {
      return true;
    }
      return obj // 👈 null and undefined check
        && Object.keys(obj).length === 0
        && Object.getPrototypeOf(obj) === Object.prototype;
  }

  function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }

  function getDefaultUserProfile() {
    // cancel-video - not interested
    // cancel-channel - don't recommend the channel
    return {
      userId: uuidv4(),
      action: 'cancel-video',  // options: 'cancel-video', 'cancel-channel'
      stats: {
      },
    }
  }

  function syncUserProfileInMixpanel(userProfile) {
    // https://developer.mixpanel.com/reference/profile-set

    const options = {
      method: 'POST',
      headers: {Accept: 'text/plain', 'Content-Type': 'application/json'},
      body: JSON.stringify([
        {
          $token: 'c6eb98ba654092d547409812229b003f',
          $distinct_id: userProfile.userId,
          $set: {
            "action": userProfile.action,
            "enableAnimation": userProfile.enableAnimation,
            "enableSearchPage": userProfile.enableSearchPage,
            "enableSuggestionsCheck": userProfile.enableSuggestionsCheck,
            "enableCommentsCheck": userProfile.enableCommentsCheck,
          },
        },
      ])
    };

    fetch('https://api-eu.mixpanel.com/engage#profile-set', options)
      .then(response => response.json())
      .then(response => console.log('stopru: Mixpanel response: ', response))
      .catch(err => console.error('stopru: Mixpanel error: ', err));
  }

  function getOrCreateUserProfile(callback) {
    if (isEmpty(userProfileCache) == false) {
      return callback(userProfileCache);
    }

    chrome.storage.sync.get(['user-profile', ], function(result) {
      console.log('stopru: get-user-profile, response:', result);
      
      let userProfile = result['user-profile']; 
      if ((typeof userProfile == "undefined") || isEmpty(userProfile)) {        
        userProfile = getDefaultUserProfile();
        chrome.storage.sync.set({'user-profile': userProfile}, function() {});
        syncUserProfileInMixpanel(userProfile);
      }
      userProfileCache = userProfile;

      callback(userProfileCache);
    });
  }

  function updateUserProfile(data, callback) {
    getOrCreateUserProfile((profile) => {
      userProfileCache = {...profile, ...data};
      chrome.storage.sync.set({'user-profile': userProfileCache}, function() {});
      syncUserProfileInMixpanel(userProfileCache);

      callback(userProfileCache); 
    });
  }

  getOrCreateUserProfile((profile) => {
    userId = profile.userId;
  });


chrome.action.onClicked.addListener(tab => {
  chrome.tabs.sendMessage(tab.id,"toggle-popup");
});

chrome.runtime.onMessage.addListener((msg, sender, resp) => {
  if (msg.command == "get-user-profile") {
    console.log('stopru: CMD, get-user-profile');

    getOrCreateUserProfile((profile) => {
      resp({
        type: 'result', 
        status: 'success', 
        data: profile, 
        request: msg,
      });
    })
    return true;
  }

  if (msg.command == "update-user-profile") {
    console.log('stopru: CMD, update-user-profile');
    
    updateUserProfile(msg.data, (profile) => {
      resp({
        type: 'result', 
        status: 'success', 
        data: profile, 
        request: msg,
      });
    })
    return;
  }

  if (msg.command == "mixnel-track") {
    console.log('stopru: CMD, mixnel-track');

    const options = {
      method: 'POST',
      headers: {Accept: 'text/plain', 'Content-Type': 'application/json'},
      body: JSON.stringify([{
        event: msg.data.eventName,
        properties: {
          token: 'c6eb98ba654092d547409812229b003f', 
          distinct_id: userId,
          ...msg.data.properties,
        },
      }])
    };

    fetch('https://api-eu.mixpanel.com/track?ip=1', options)
      .then(response => response.json())
      .then(response => console.log('stopru: Mixpanel response: ', response))
      .catch(err => console.error('stopru: Mixpanel error: ', err));

    resp({
      type: 'result',
      status: 'success',
      data: {},
      request: msg,
    });
    return true;
  }

  if (msg.command == "mixnel-increment") {
    console.log('stopru: CMD, mixnel-increment', msg.data);

    const options = {
      method: 'POST',
      headers: {Accept: 'text/plain', 'Content-Type': 'application/json'},
      body: JSON.stringify([
        {$token: 'c6eb98ba654092d547409812229b003f', $distinct_id: userId, $add: {videosTotal: msg.data.videosTotal}},
        {$token: 'c6eb98ba654092d547409812229b003f', $distinct_id: userId, $add: {videosRu: msg.data.videosRu}}
      ])
    };

    fetch('https://api-eu.mixpanel.com/engage#profile-numerical-add', options)
      .then(response => response.json())
      .then(response => console.log('stopru: Mixpanel response: ', response))
      .catch(err => console.error('stopru: Mixpanel error: ', err));
    
    resp({
      type: 'result',
      status: 'success',
      data: {},
      request: msg,
    });
    return true;
  }

  if (msg.command == "create-video-blocked") {
    console.log('stopru: CMD, create-video-blocked', );

    let data = {
      createdAt: Date.now(),
      ...msg.data
    };

    chrome.storage.local.get(['videos-blocked', ], function(result) {
      let videosBlocked = result['videos-blocked'] || [];
      videosBlocked.unshift(data);

      let videosBlockedLength = videosBlocked.length;
      console.log('stopru: videosBlockedLength', videosBlockedLength, videosBlocked);
      if (videosBlockedLength >= LIMIT_UPPER) {
        console.log('stopru: going to trim array');
        videosBlocked = videosBlocked.slice(0, LIMIT_LOWER);
        console.log('stopru: videosBlocked', videosBlocked);
        getOrCreateUserProfile((profile) => {
          profile.statsVideosBlockedCount = (profile.statsVideosBlockedCount || 0) + LIMIT_LOWER;
          updateUserProfile({'statsVideosBlockedCount': profile.statsVideosBlockedCount}, () => {});
        });
      }

      chrome.storage.local.set({'videos-blocked': videosBlocked}, function() {});
    });
    resp({
      type: 'result',
      status: 'success',
      data: {},
      request: msg,
    });
    return true;
  }

  if (msg.command == "create-channel-blocked") {
    console.log('stopru: CMD, create-channel-blocked', );
    let data = {
      createdAt: Date.now(),
      ...msg.data
    };

    chrome.storage.local.get(['channels-blocked', ], function(result) {
      let channelsBlocked = result['channels-blocked'] || [];
      channelsBlocked.unshift(data);

      let channelsBlockedLength = channelsBlocked.length;
      if (channelsBlockedLength >= LIMIT_UPPER) {
        channelsBlocked = channelsBlocked.slice(0, LIMIT_LOWER);

        getOrCreateUserProfile((profile) => {
          profile.statsChannelsBlockedCount = (profile.statsChannelsBlockedCount || 0) + LIMIT_LOWER;
          updateUserProfile({'statsChannelsBlockedCount': profile.statsChannelsBlockedCount}, () => {});
        });
      }

      chrome.storage.local.set({'channels-blocked': channelsBlocked}, function() {});      
    });
    resp({
      type: 'result',
      status: 'success',
      data: {},
      request: msg,
    });
    return true;
  }

  if (msg.command == "close-popup") {

    chrome.tabs.query({ active: true }, function(tabs){
      for (let tab of tabs) {
        chrome.tabs.sendMessage(tab['id'], "close-popup");
      }
    });

    return true;
  }

  if (msg.command == "enable-settings-ui") {
    console.log('stopru: CMD, enable-settings-ui');

    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {command: 'enable-settings-ui', data: msg.data}, (response) => {
        console.log('stopru: enable-settings-ui, response:', response);
      });
    });

    resp({
      type: 'result',
      status: 'success',
      data: {},
      request: msg,
    });
    return true;
  }

  console.log('stopru: Unknown command', msg.command);
});


} catch(e) {
  console.log('stopru: Error', e);
}

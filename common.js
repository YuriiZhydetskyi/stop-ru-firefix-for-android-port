const LOG_ENABLED = false;

function twoDigits(n) {
    return n < 10 ? '0' + n : n;
}

function threeDigits(n) {
    if (n < 10) {
        return '00' + n;
    }

    if (n < 100) {
        return '0' + n;
    }

    return n;
}

function getDateTime(withDay=false) {
    let datetime = new Date();

    // let month = twoDigits(datetime.getMonth() + 1);
    // let year = datetime.getFullYear().toString().slice(2, 4);

    let hours = twoDigits(datetime.getHours());
    let minutes = twoDigits(datetime.getMinutes());
    let seconds = twoDigits(datetime.getSeconds());
    let miliseconds = threeDigits(datetime.getMilliseconds());

    if (withDay == true) {
        let day = twoDigits(datetime.getDate());
        return day + '-' + hours + ':' + minutes + ':' + seconds + ':' + miliseconds;
    }

    // day + '.' + month + '.' + year + '-' + 
    return hours + ':' + minutes + ':' + seconds + ':' + miliseconds;
}

function log(...args) {
    if(LOG_ENABLED == true) {
        console.log(`stopRU ${getDateTime()}:`, ...args);
    }
}

function getYTKMode() {
    if (typeof ytkMode === "undefined") {
        return null;
    } else {
        return ytkMode;
    }
}

function getEventMetaData() {
    return {
        "url": window.location.href,
        "version": chrome.runtime.getManifest().version,
        "ytkMode": getYTKMode(),
        ...getBrowserData(),
    }
}

let brovserInfo = null;
function getBrowserData() {
    if (brovserInfo == null) {
        // https://stackoverflow.com/a/16938481/4191904
        try {
            var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
            if(/trident/i.test(M[1])){
                tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
                return {
                    browserName:'IE',
                    browserVersion: (tem[1]||''),
                };
            }
            if(M[1]==='Chrome'){
                tem=ua.match(/\bOPR|Edge\/(\d+)/)
                if(tem!=null)   {
                    return {
                        browserName:'Opera',
                        browserVersion: tem[1],
                    };
                }
            }
            M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
            
            brovserInfo = {
                browserName: M[0],
                browserVersion: M[1]
            }
        } catch(e) {
            brovserInfo = {
                browserName: '0',
                browserVersion: '0',
                browserError: e,
            }
        }
    }

    return brovserInfo;
 }

 function setCookie(cname, cvalue, exdexpireDate) {
    let expires = "expires=" + exdexpireDate.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

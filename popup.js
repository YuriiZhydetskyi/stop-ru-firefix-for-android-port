let ITEMS_PER_PAGE = 3;

function renderUI() {
  document.getElementById('popup-close-btn').addEventListener('click', popupClose);

  // videos
  document.getElementById('tab-removed-videos').addEventListener('click', showRemovedVideos);
  document.getElementById('removed-video-list-pagination-left').addEventListener('click', removedVideoListPaginationLeft, false);
  document.getElementById('removed-video-list-pagination-right').addEventListener('click', removedVideoListPaginationRight, false);

  // channels
  document.getElementById('tab-removed-channels').addEventListener('click', showRemovedChannels);
  document.getElementById('removed-channel-list-pagination-left').addEventListener('click', removedChannelListPaginationLeft, false);
  document.getElementById('removed-channel-list-pagination-right').addEventListener('click', removedChannelListPaginationRight, false);


  document.getElementById('how-it-works-open').addEventListener('click', howItWorksShow);
  document.getElementById('how-it-works-close').addEventListener('click', howItWorksHide);
  
  document.getElementById('donate-btn').addEventListener('click', showDonate);
  document.getElementById('donate-btn-up').addEventListener('click', showDonate);


  // whitelist
  document.getElementById('whitelist-channel-add').addEventListener('click', whiteListChannelAdd);
  document.getElementById('whitelist-pagination-left').addEventListener('click', whitelistPaginationLeft, false);
  document.getElementById('whitelist-pagination-right').addEventListener('click', whitelistPaginationRight, false);

  // document.getElementById('copy-link-btn').addEventListener('click', copyLink);

  // document.getElementById('click-btn').addEventListener('click', clickBtn);

  document.querySelectorAll("input[name='botAction']").forEach((input) => {
    input.addEventListener('change', (event) => {
      updateSettingsBE({action: event.target.value});
    });
  });

  document.getElementById('settings-enable-animation').addEventListener('change', (event) => {
    let enableAnimation = '';
    if (event.target.checked == true) {
      enableAnimation = 'a-enabled';
    } else {
      enableAnimation = 'a-disabled';
    }

    updateSettingsBE({'enableAnimation': enableAnimation});
    updateSettingsUI({'enableAnimation': enableAnimation});
  });

  document.getElementById('settings-enable-search-page').addEventListener('change', (event) => {
    let enableSearchPage = '';
    if (event.target.checked == true) {
      enableSearchPage = 'sp-enabled';
    } else {
      enableSearchPage = 'sp-disabled';
    }

    updateSettingsBE({'enableSearchPage': enableSearchPage});
    updateSettingsUI({'enableSearchPage': enableSearchPage});
  });

  document.getElementById('settings-enable-suggestions-video-page').addEventListener('change', (event) => {
    let enableSuggestionsCheck = '';
    if (event.target.checked == true) {
      enableSuggestionsCheck = 'vp-suggestions-enabled';
    } else {
      enableSuggestionsCheck = 'vp-suggestions-disabled';
    }

    updateSettingsBE({'enableSuggestionsCheck': enableSuggestionsCheck});
    updateSettingsUI({'enableSuggestionsCheck': enableSuggestionsCheck});
  });

  document.getElementById('settings-enable-comments-video-page').addEventListener('change', (event) => {
    let enableCommentsCheck = '';
    if (event.target.checked == true) {
      enableCommentsCheck = 'vp-comments-enabled';
    } else {
      enableCommentsCheck = 'vp-comments-disabled';
    }

    updateSettingsBE({'enableCommentsCheck': enableCommentsCheck});
    updateSettingsUI({'enableCommentsCheck': enableCommentsCheck});
  });

  listVideos(pageNumber=1);
  listChannels(pageNumber=1);

  listWhitelist(pageNumber=1);  // i am not sure is we have to render it here.
  showStats(listName='videos-blocked');

  var postData = {
    eventName: 'popupOpen',
    properties: {
      ...getEventMetaData(),
    },
  }
  chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
    log('popup-open -> mixnel-track response', response);
  });

  document.getElementById("verion-number").innerText = chrome.runtime.getManifest().version;
}

window.onload = renderUI;


function whiteListChannelDelete(event) {
  let channelName = event.currentTarget.channelName;
  if (window.confirm(`Ви справді бажаєте видалити канал [${channelName}] з Білого спику?`)) {
    console.log('yes');
    // delete channel from DB locally
    // render WhiteList again

    chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {
      let whitelist = response.data['whitelist-channels'] || [];
      whitelist = whitelist.filter(function(el) { log(el, el.channelName, channelName); return el.channelName != channelName; });

      updateSettingsBE({"whitelist-channels": whitelist}, () => {
        log('whitelist updated');
        listWhitelist(pageNumber=1);
      });

    });

    var postData = {
      eventName: 'whiteListEvent',
      properties: {
        ...getEventMetaData(),
        "message": "WL channel removed",
        "channelName": channelName,
      },
    };
    chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
      log('mixnel-track, response -> popupButtonDonate', response);
    });

  } else {
    console.log('no');
  }
}

function whiteListChannelAdd() {
  let channelName = window.prompt("Вкажіть точну назву каналу, наприклад [Крым.Реалии]");

  if (channelName == null) {
    return;
  }

  function validateChannelName(name) {
    return name && name.length > 0;
  }
  
  if (validateChannelName(channelName) == true) {
    if (window.confirm(`Ви справді бажаєте додати канал [${channelName}] до Білого спику?`)) {
      console.log('yes');
      // store channel in DB locally
      // render WhiteList again

      let newChannel = {
        "channelName": channelName,
        "createdAt": formatDatetime(Date.now()),
      }

      chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {
        let whitelist = response.data['whitelist-channels'] || [];
        console.log('stopru: whitelist-channels, response:', whitelist);

        whitelist.push(newChannel);
        
        updateSettingsBE({"whitelist-channels": whitelist}, () => {
          log('whitelist updated');
          listWhitelist(pageNumber=1);
        });
      });

      var postData = {
        eventName: 'whiteListEvent',
        properties: {
          ...getEventMetaData(),
          "message": "WL channel added",
          "channelName": channelName,
        },
      };
      chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
        log('mixnel-track, response -> popupButtonDonate', response);
      });
      
    } else {
      console.log('no');
    }

  } else {
    alert(`Назва каналу [${channelName}] не є валідною.`);
  }
  
}


function popupClose() {
  chrome.runtime.sendMessage({command: 'close-popup', data: {}}, (response) => {
    log('close-popup -> response', response);
  });
}

function addDays(date, number) {
  const newDate = new Date(date);
  return new Date(newDate.setDate(newDate.getDate() + number));
}

function showStats(listName) {
  chrome.storage.sync.get(['user-profile', ], function(result) {
    let userProfile = result['user-profile'] || {};
    
    chrome.storage.local.get([listName, ], function(result) {
      let objectsBlocked = result[listName] || [];
      
      let startDate = addDays(Date.now(), -6);
      startDate.setHours(0);
      startDate.setMinutes(0);
      startDate.setSeconds(0);
      startDate.setMilliseconds(0);

      let objectsToShow = objectsBlocked.filter(o => o.createdAt > startDate);

      let videosDataToShow = {};

      for(let o of objectsToShow) {
        let d = formatDatetime(o.createdAt).split(' ')[0];
        
        if (videosDataToShow[d] === undefined) {
          videosDataToShow[d] = 1;
        } else {
          videosDataToShow[d] += 1;
        }
      }
      
      let maxValue = Math.max(...Object.values(videosDataToShow));
      let step = (maxValue > 4 ) ? 110 / maxValue : 10;

      for (let i = 0; i < 7; i++) {
        let d = formatDatetime(addDays(startDate, i)).split(' ')[0];
        let v = videosDataToShow[d] || 0;
        document.getElementById(`bar-${i}`).setAttribute("style",`height:${v * step}px`);
        document.getElementById(`bar-${i}`).setAttribute("title", `${v} відео, ${d}`);
      }

      let valueFromUserProfile = 0;
      if (listName == 'videos-blocked') {
        valueFromUserProfile = userProfile.statsVideosBlockedCount || 0;  
      } else {
        valueFromUserProfile = userProfile.statsChannelsBlockedCount || 0;
      }

      document.getElementById('stat-total-objects').innerText = objectsBlocked.length + valueFromUserProfile;
    });
  });
}


function removedVideoListPaginationLeft() {
  let currentPage = parseInt(document.getElementById('removed-video-page').innerText);
  currentPage -= 1;
  document.getElementById('removed-video-page').innerText = currentPage;

  listVideos(pageNumber=currentPage);

  var postData = {
    eventName: 'popupPaginationLeft',
    properties: {
      ...getEventMetaData(),
    },
  }
  chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
    log('removed-video-list-pagination-left -> mixnel-track response', response);
  });
}

function removedChannelListPaginationLeft() {
  let currentPage = parseInt(document.getElementById('removed-channel-page').innerText);
  currentPage -= 1;
  document.getElementById('removed-channel-page').innerText = currentPage;

  listChannels(pageNumber=currentPage);

  var postData = {
    eventName: 'popupChannelPaginationLeft',
    properties: {
      ...getEventMetaData(),
    },
  }
  chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
    log('removed-channel-list-pagination-left -> mixnel-track response', response);
  });
}

function removedVideoListPaginationRight() {
  let currentPage = parseInt(document.getElementById('removed-video-page').innerText);
  currentPage += 1;
  document.getElementById('removed-video-page').innerText = currentPage;

  listVideos(pageNumber=currentPage);

  var postData = {
    eventName: 'popupPaginationRight',
    properties: {
      ...getEventMetaData(),
    },
  }
  chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
    log('removed-video-list-pagination-right -> mixnel-track response', response);
  });
}

function removedChannelListPaginationRight() {
  let currentPage = parseInt(document.getElementById('removed-channel-page').innerText);
  currentPage += 1;
  document.getElementById('removed-channel-page').innerText = currentPage;

  listChannels(pageNumber=currentPage);

  var postData = {
    eventName: 'popupChannelPaginationRight',
    properties: {
      ...getEventMetaData(),
    },
  }
  chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
    log('removed-channel-list-pagination-right -> mixnel-track response', response);
  });
}

function twoDigits(n) {
  return n < 10 ? '0' + n : n;
}

function formatDatetime(datetime) {

  if (datetime) {
    datetime = new Date(datetime);

    let day = twoDigits(datetime.getDate());
    let month = twoDigits(datetime.getMonth() + 1);
    let year = datetime.getFullYear().toString().slice(2, 4);

    let hours = twoDigits(datetime.getHours());
    let minutes = twoDigits(datetime.getMinutes());

    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
  }
  return '';
}

function cleanText(text) {
  return text && text.trim().replace(/(\r\n|\n|\r)/gm, "");
}

function renderNoElements(text) {
  let textElemenet = document.createElement('p');
  textElemenet.className = "no-elements";
  textElemenet.innerText = text;

  let divElement = document.createElement('div');
  divElement.className = "removed-video-item f-column no-elements";
  divElement.appendChild(textElemenet);
  
  return divElement;
}

function renderVideoItem(element) {
  let videoName = document.createElement('a');
  videoName.className = "video-name";
  videoName.setAttribute("target", "_blank");
  videoName.setAttribute("title", cleanText(element.videoName));
  if (element.videoId.includes('www.youtube.com')) {
    videoName.href = element.videoId;
  } else {
    videoName.href = "https://www.youtube.com/watch?v=" + element.videoId;
  }

  videoName.innerText = cleanText(element.videoName);

  let videoDuration = document.createElement('p');
  videoDuration.className = "duration";
  videoDuration.innerText = cleanText(element.videoDuration);

  let divVideoUpper = document.createElement('div');
  divVideoUpper.className = "removed-video-item-upper f-row f-space-between";
  divVideoUpper.appendChild(videoName);
  divVideoUpper.appendChild(videoDuration);


  let channelName = document.createElement('a');
  channelName.className = "channel-name";
  channelName.setAttribute("target", "_blank");
  channelName.setAttribute("title", cleanText(element.channelName));
  if (element.channelId) {
    channelName.href = "https://www.youtube.com" + element.channelId;
  } else {
    channelName.href = "https://www.youtube.com/results?search_query=" + cleanText(element.channelName);
  }
  channelName.innerText = cleanText(element.channelName);

  let videoDatetime = document.createElement('p');
  videoDatetime.className = "datetime";
  videoDatetime.innerText = formatDatetime(element.createdAt);

  let divVideoLower = document.createElement('div');
  divVideoLower.className = "removed-video-item-lower f-row f-space-between";
  divVideoLower.appendChild(channelName);
  divVideoLower.appendChild(videoDatetime);


  let divVideo = document.createElement('div');
  divVideo.className = "removed-video-item f-column";
  divVideo.appendChild(divVideoUpper);
  divVideo.appendChild(divVideoLower);
  
  return divVideo;
}

function listVideos(pageNumber) {
  chrome.storage.local.get(['videos-blocked', ], function(result) {
    let videosBlocked = result['videos-blocked'] || [];

    log('videosBlocked', videosBlocked);

    let startItem = (pageNumber - 1) * ITEMS_PER_PAGE;
    let totalPages = Math.ceil(videosBlocked.length / ITEMS_PER_PAGE);
    document.getElementById('removed-video-pages-all').innerText = totalPages;
    data = videosBlocked.slice(startItem, startItem + ITEMS_PER_PAGE);

    document.getElementById('videos-items').innerHTML = '';

    if (data.length > 0) {
      data.forEach((element) => {
          document.getElementById('videos-items').appendChild(renderVideoItem(element));
      });
    } else if (pageNumber == 1) {
      document.getElementById('videos-items').appendChild(renderNoElements('У вас наразі немає видалених відео.'));
    }

    if (pageNumber == 1) {
      document.getElementById('removed-video-list-pagination-left').disabled = true;
    } else {
      document.getElementById('removed-video-list-pagination-left').disabled = false;
    }

    if (pageNumber >= totalPages) {
      document.getElementById('removed-video-list-pagination-right').disabled = true;
    } else {
      document.getElementById('removed-video-list-pagination-right').disabled = false;
    }
  });
}

function listChannels(pageNumber) { 
  chrome.storage.local.get(['channels-blocked', ], function(result) {
    let channelsBlocked = result['channels-blocked'] || [];
    let startItem = (pageNumber - 1) * ITEMS_PER_PAGE;
    let totalPages = Math.ceil(channelsBlocked.length / ITEMS_PER_PAGE);
    document.getElementById('removed-channel-pages-all').innerText = totalPages;
    data = channelsBlocked.slice(startItem, startItem + ITEMS_PER_PAGE);

    document.getElementById('channels-items').innerHTML = '';

    if (data.length > 0) {
      data.forEach((element) => {
          document.getElementById('channels-items').appendChild(renderVideoItem(element));
      });
    } else if (pageNumber == 1) {
      document.getElementById('channels-items').appendChild(renderNoElements('У вас наразі немає видалених каналів.'));
    }

    if (pageNumber == 1) {
      document.getElementById('removed-channel-list-pagination-left').disabled = true;
    } else {
      document.getElementById('removed-channel-list-pagination-left').disabled = false;
    }

    if (pageNumber >= totalPages) {
      document.getElementById('removed-channel-list-pagination-right').disabled = true;
    } else {
      document.getElementById('removed-channel-list-pagination-right').disabled = false;
    }
  });
}

function renderWhitelistItem(element) {
  // let imgChannel = document.createElement('img');
  // imgChannel.className = "whitelist-channel-logo";

// main section

  let aChannelName = document.createElement('a');
  aChannelName.className = "video-name";
  aChannelName.setAttribute("target", "_blank");
  aChannelName.setAttribute("title", cleanText(element.channelName));
  aChannelName.href = "https://www.youtube.com/results?search_query=" +  cleanText(element.channelName);
  aChannelName.innerText = cleanText(element.channelName);

  let divChannelUpper = document.createElement('div');
  divChannelUpper.className = "removed-video-item-upper f-row f-space-between";
  divChannelUpper.appendChild(aChannelName);


  let pChannelId = document.createElement('p');
  pChannelId.className = "whitelist-channel-name";
  pChannelId.innerText = cleanText(element.createdAt);

  let divChannelLower = document.createElement('div');
  divChannelLower.className = "removed-video-item-lower f-row f-space-between";
  divChannelLower.appendChild(pChannelId);

  let divMainSection = document.createElement('div');
  divMainSection.className = "f-column w-100-percent";
  divMainSection.appendChild(divChannelUpper);
  divMainSection.appendChild(divChannelLower);


// button
  let svgRemove = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svgRemove.setAttribute("class","icon-btn");
  svgRemove.setAttribute("viewBox", "0 0 32 32");

  let svgPath1 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  let svgPath2 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  let svgPath3 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  let svgPath4 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  let svgPath5 = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  svgPath1.setAttribute("d","M20,29H12a5,5,0,0,1-5-5V12a1,1,0,0,1,2,0V24a3,3,0,0,0,3,3h8a3,3,0,0,0,3-3V12a1,1,0,0,1,2,0V24A5,5,0,0,1,20,29Z");
  svgPath2.setAttribute("d","M26,9H6A1,1,0,0,1,6,7H26a1,1,0,0,1,0,2Z");
  svgPath3.setAttribute("d","M20,9H12a1,1,0,0,1-1-1V6a3,3,0,0,1,3-3h4a3,3,0,0,1,3,3V8A1,1,0,0,1,20,9ZM13,7h6V6a1,1,0,0,0-1-1H14a1,1,0,0,0-1,1Z");
  svgPath4.setAttribute("d","M14,23a1,1,0,0,1-1-1V15a1,1,0,0,1,2,0v7A1,1,0,0,1,14,23Z");
  svgPath5.setAttribute("d","M18,23a1,1,0,0,1-1-1V15a1,1,0,0,1,2,0v7A1,1,0,0,1,18,23Z");
  svgRemove.appendChild(svgPath1);
  svgRemove.appendChild(svgPath2);
  svgRemove.appendChild(svgPath3);
  svgRemove.appendChild(svgPath4);
  svgRemove.appendChild(svgPath5);

  let buttonRemove = document.createElement('button');
  buttonRemove.className = "button-icon";
  buttonRemove.setAttribute("title", "Видалити канал з Білого списку");
  buttonRemove.setAttribute("id", "whitelist-channel-delete");
  buttonRemove.addEventListener('click', whiteListChannelDelete);
  buttonRemove.channelName = element.channelName;
  buttonRemove.appendChild(svgRemove);

// final composition
  let divVideo = document.createElement('div');
  divVideo.className = "removed-video-item f-row v-center";
  // divVideo.appendChild(imgChannel);
  divVideo.appendChild(divMainSection);
  divVideo.appendChild(buttonRemove);
  
  return divVideo;
}

function listWhitelist(pageNumber) {
  chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {        
    let channels = response.data['whitelist-channels'] || [];

    channels = channels.reverse();

    log('listWhitelist', channels);
    let startItem = (pageNumber - 1) * ITEMS_PER_PAGE;
    let totalPages = Math.ceil(channels.length / ITEMS_PER_PAGE);
    document.getElementById('whitelist-pages-all').innerText = totalPages;
    document.getElementById('whitelist-page').innerText = pageNumber;
    data = channels.slice(startItem, startItem + ITEMS_PER_PAGE);

    document.getElementById('whitelist-items').innerHTML = '';

    if (data.length > 0) {
      data.forEach((element) => {
          document.getElementById('whitelist-items').appendChild(renderWhitelistItem(element));
      });
    } else if (pageNumber == 1) {
      document.getElementById('whitelist-items').appendChild(renderNoElements('Білий список порожній.'));
    }

    if (pageNumber == 1) {
      document.getElementById('whitelist-pagination-left').disabled = true;
    } else {
      document.getElementById('whitelist-pagination-left').disabled = false;
    }

    if (pageNumber >= totalPages) {
      document.getElementById('whitelist-pagination-right').disabled = true;
    } else {
      document.getElementById('whitelist-pagination-right').disabled = false;
    }
  });
}


function whitelistPaginationLeft() {
  let currentPage = parseInt(document.getElementById('whitelist-page').innerText);
  currentPage -= 1;
  document.getElementById('whitelist-page').innerText = currentPage;

  listWhitelist(pageNumber=currentPage);
}

function whitelistPaginationRight() {
  let currentPage = parseInt(document.getElementById('whitelist-page').innerText);
  currentPage += 1;
  document.getElementById('whitelist-page').innerText = currentPage;

  listWhitelist(pageNumber=currentPage);
}

function updateSettingsBE(settingsToUpdate, callback) {
  chrome.runtime.sendMessage({command: 'update-user-profile', data: settingsToUpdate}, (response) => {
    log('update-user-profile -> response', response);
    if (callback) {
      callback();
    }
  });
}

function updateSettingsUI(settingsToUpdate) {
  chrome.runtime.sendMessage({command: 'enable-settings-ui', data: settingsToUpdate}, (response) => {
    log('enable-settings-ui -> response', response);
  });
}

function showSettings() {
  chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {
    log('get-user-profile -> response', response);

    if (['cancel-video', 'cancel-channel', 'highlight-video'].includes(response.data.action)) {
      let element = document.getElementById(response.data.action);
      if (element) {
        element.checked = true;
      }
    }

    let settingEnableAnimation = document.getElementById('settings-enable-animation');
    if (response.data.enableAnimation == 'a-disabled') {
      settingEnableAnimation.checked = false;
    } else {
      settingEnableAnimation.checked = true;
    }

    let settingEnableSearchPage = document.getElementById('settings-enable-search-page');
    if (response.data.enableSearchPage == 'sp-disabled') {
      settingEnableSearchPage.checked = false;
    } else {
      settingEnableSearchPage.checked = true;
    }

    let settingEnableSuggestionsCheck = document.getElementById('settings-enable-suggestions-video-page');
    if (response.data.enableSuggestionsCheck == 'vp-suggestions-disabled') {
      settingEnableSuggestionsCheck.checked = false;
    } else {
      settingEnableSuggestionsCheck.checked = true;
    }

    let settingEnableCommentsCheck = document.getElementById('settings-enable-comments-video-page');
    if (response.data.enableCommentsCheck == 'vp-comments-disabled') {
      settingEnableCommentsCheck.checked = false;
    } else {
      settingEnableCommentsCheck.checked = true;
    }
  });
}

function howItWorksShow() {
  document.getElementById('how-it-works-body').style.display = "block";
  document.getElementById('how-it-works-open').style.display = "none";
  document.getElementById('how-it-works-close').style.display = "block";
  
  showSettings();
}

function howItWorksHide() {
  document.getElementById('how-it-works-body').style.display = "none";
  document.getElementById('how-it-works-open').style.display = "block";
  document.getElementById('how-it-works-close').style.display = "none";
}

function showDonate() {
  // window.open('https://www.patreon.com/stopRU','_blank');

  if (document.getElementById('donate-section').style.display == "none") {
    log('show');
    document.getElementById('donate-section').style.display = "block";
    document.getElementById('donate-btn-up').style.display = "block";

    var postData = {
      eventName: 'popupButtonDonate',
      properties: {
        ...getEventMetaData(),
      },
    }
    chrome.runtime.sendMessage({command: 'mixnel-track', data: postData}, (response) => {
      log('mixnel-track, response -> popupButtonDonate', response);
    });
  } else {
    log('hide');
    document.getElementById('donate-section').style.display = "none";
    document.getElementById('donate-btn-up').style.display = "none";
  }
}

function showRemovedVideos() {
  document.getElementById("tab-removed-videos").classList.add("container-tab-active");
  document.getElementById("tab-removed-channels").classList.remove("container-tab-active");

  document.getElementById("container-removed-videos").style.display = "block";
  document.getElementById("container-removed-channels").style.display = "none";
  showStats(listName='videos-blocked');
}


function showRemovedChannels() {
  document.getElementById("tab-removed-videos").classList.remove("container-tab-active");
  document.getElementById("tab-removed-channels").classList.add("container-tab-active");

  document.getElementById("container-removed-videos").style.display = "none";
  document.getElementById("container-removed-channels").style.display = "block";
  showStats(listName='channels-blocked');
}

function copyLink() {
  navigator.clipboard.writeText('https://chrome.google.com/webstore/detail/stopru/adjmjjpnbddnlcmcjmlipkbldndkhhaj').then(() => {
    alert("successfully copied");
  }).catch((e) => {
    alert("something went wrong", e);
  });
  // alert("Посилання скопійовано.");
}


function clickBtn() {
}

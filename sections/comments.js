function consumeElementComment(commentThreadRenderer) {
    let queryResult = commentThreadRenderer.querySelectorAll('ytd-comment-renderer div[id=content]');
    if (queryResult.length > 0) {
        let text = queryResult[0].innerText.trim();
        if (checkText(text) == true) {
            commentThreadRenderer.classList.add("stopru-video-comment");
        }
    }
}

const configComment = { attributeFilter: [ "src", ], subtree: true }

const callbackComment = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let authorThumbnailElement = mutation.target.parentElement.parentElement.parentElement;
                if (authorThumbnailElement.getAttribute('id') != 'author-thumbnail') {
                    continue;
                }

                let commentRenderer = mutation.target.closest('ytd-comment-renderer');
                if (!commentRenderer) {
                    continue;
                }
                if (commentRenderer.hasAttribute('is-reply')) {
                    continue;                    
                }
                    
                let commentThreadRenderer = mutation.target.closest('ytd-comment-thread-renderer');
                commentThreadRenderer.classList.remove("stopru-video-comment");
                consumeElementComment(commentThreadRenderer);
            } catch(e) { log('Error in callbackComment', e); }
        }
        
    }
};

const observerComment = new MutationObserver(callbackComment);


function initCheckComment(parent) {
    let commentThreadRenderers = parent.querySelectorAll("ytd-comment-thread-renderer");
    log(`initCheckComment -> found ${commentThreadRenderers.length} comments`);

    for(let commentThreadRenderer of commentThreadRenderers) {
        consumeElementComment(commentThreadRenderer);
    }

    observerComment.observe(parent, configComment);
}


function findComments(node, findCommentsCounter) {
    if (findCommentsCounter < 0) {
        trackError(message="findComments-counter-0");
        return;
    }

    let queryResult = node.querySelectorAll('div[id=primary] ytd-comments');
    if (queryResult.length > 0) {
        initCheckComment(parent=queryResult[0]);
        return;

    } else {
        log('findComments -> findCommentsCounter', findCommentsCounter);
        setTimeout(findComments, 2000, node, findCommentsCounter - 1);
        return;
    }
}

function getVideoDescriptionOnSearchPage(element) {
    // it works on search page, at least

    let selectorResult = element.querySelectorAll('.metadata-snippet-container, .metadata-snippet-container-one-line');
    if (selectorResult.length > 0) {
        return selectorResult[0].innerText.trim();
    }
    return '';
}

function consumeElementSearch(element) {
    if (checkElement(element, getVideoDescriptionOnSearchPage(element)) == true) {
        // check channel name + video name + video description
        element.classList.add("stopru-video-search");
    }
}

const configSearch = { attributeFilter: [ "src", ], subtree: true }

const callbackSearch = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        let parent = mutation.target.parentElement;
        if ((parent && parent.parentElement && parent.parentElement.getAttribute('id') == 'thumbnail') && (mutation.target.getAttribute('src') != null)) {
            let element = mutation.target.closest('ytd-video-renderer, ytd-reel-item-renderer');

            if (element != null) {
                element.classList.remove("stopru-video-search");
                consumeElementSearch(element);
            }
        }
    }
};

const observerSearch = new MutationObserver(callbackSearch);

function initCheckSearch(parent) {
    var elements = parent.querySelectorAll("ytd-video-renderer, ytd-reel-item-renderer");
    log(`initCheckSearch -> found ${elements.length} elements`);

    for(let element of elements) {
        consumeElementSearch(element);
    }

    const targetNode = parent;
    observerSearch.observe(targetNode, configSearch);
}

function findElementsSearch(parent, findElementsSearchCounter) {
    if (findElementsSearchCounter && findElementsSearchCounter <= 0) {  // we found some element
        trackError(message="findElementsSearch-counter-0");
        return;
    }

    // elements are grouped under ytd-item-section-renderer
    if (parent.querySelectorAll('[id="contents"]').length > 0) {
        initCheckSearch(parent=parent.querySelectorAll('[id="contents"]')[0]);
        return;

    } else {
        log('findElementsSearch -> findElementsSearchCounter', findElementsSearchCounter);
        setTimeout(findElementsSearch, 100, parent, findElementsSearchCounter - 1);
        return;
    }
}

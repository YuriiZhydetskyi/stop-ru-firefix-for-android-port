const configSideSuggestion = { attributeFilter: [ "src", ], subtree: true }

const callbackSideSuggestion = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let videoThumbnailElement = mutation.target.parentElement.parentElement;
                if (videoThumbnailElement.getAttribute('id') != 'thumbnail') {
                    continue;
                }

                let element = mutation.target.closest('ytd-compact-video-renderer');
                consumeElement(element);

            } catch(e) { log('Error in callbackSideSuggestion', e); }
        }
        
    }
};

const observerSideSuggestion = new MutationObserver(callbackSideSuggestion);


function initCheckSideSuggestion(parent) {
    let sideSuggestions = parent.querySelectorAll("ytd-compact-video-renderer");
    log(`initCheckSideSuggestion -> found ${sideSuggestions.length} sideSuggestions`);

    for(let element of sideSuggestions) {
        try {
            let image = element.querySelector("[id='thumbnail'] img");
            if (image && image.getAttribute('src')) {
                consumeElement(element);
            }
        } catch(error) {
            trackError(message="initCheckSideSuggestion", info={"error": error.stack.toString()});
        }
        // log('initCheckSuggestion -> ', getVideoName(sideSuggestion));
        // sideSuggestion.style.border = 'solid orange';
    }

    observerSideSuggestion.observe(parent, configSideSuggestion);
}


function findSideSuggestions(node, findSideSuggestionsCounter) {
    if (!document.body.classList.contains('stopru-suggestions')) {
        log('findSideSuggestions - disabled');
        return;
    }

    if (findSideSuggestionsCounter < 0) {
        let divPrimay = node.querySelector('div[id=primary]');
        let ytdResultRender = node.querySelector('ytd-watch-next-secondary-results-renderer');
        let divItems = node.querySelector('ytd-watch-next-secondary-results-renderer div[id=items]');

        try {
            trackError(message="findSideSuggestions-counter-0", info={
                "div-primay": divPrimay && divPrimay.innerHTML.slice(0, 100).trim(),
                "ytd-watch-next-secondary-results-renderer": ytdResultRender && ytdResultRender.innerHTML.slice(0, 100).trim(),
                "div-items": divItems && divItems.innerHTML.slice(0, 100).trim(),
                "isIFrame": (window === window.parent) ? false : true,
                "parentURL": (window.location != window.parent.location) ? document.referrer : document.location.href,
            });
        } catch(error) {
            trackError(message="findSideSuggestions-error", info={"error": error.stack.toString()});
        }

        return;
    }

    let queryResult = node.querySelectorAll('ytd-watch-next-secondary-results-renderer div[id=items]');
    if (queryResult.length > 0) {
        log('findSideSuggestions -> counter', findSideSuggestionsCounter);
        stepFindElementSideSuggestion = getStepLog(stepName='stepFindElementSide', stepCounter=findSideSuggestionsCounter);
        initCheckSideSuggestion(parent=queryResult[0]);
        return;

    } else {
        log('findSideSuggestions -> findSideSuggestionsCounter', findSideSuggestionsCounter);
        let delay = 4000;
        if (findSideSuggestionsCounter < 10) {
            delay = delay * 5;
        }
        setTimeout(findSideSuggestions, delay, node, findSideSuggestionsCounter - 1);
        return;
    }
}

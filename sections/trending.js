
function getVideoDescriptionOnTrendingPage(element) {
    // it works on trending page, at least
    let selectorResult = element.querySelectorAll('#description-text');
    if (selectorResult.length > 0) {
        return selectorResult[0].innerText.trim();
    }
    return '';
}

function consumeTrendingElement(element) {
    if (checkElement(element, getVideoDescriptionOnTrendingPage(element)) == true) {
        // check channel name + video name + video description
        element.classList.add("stopru-video-trending");
    }
}

const configTrending = { attributeFilter: [ "src", ], subtree: true }

const callbackTrending = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let thumbnailElement = mutation.target.parentElement.parentElement;
                if (thumbnailElement.getAttribute('id') == 'thumbnail') {
                    let element = mutation.target.closest('ytd-video-renderer');

                    if (element != null) {
                        element.classList.remove("stopru-video-trending");
                        consumeTrendingElement(element);
                    }
                }
            } catch(e) { log('Error in callbackTrending', e); }
        }
    }
};

const observerTrending = new MutationObserver(callbackTrending);


function initCheckTrendingElements(parent) {
    let trendingElements = parent.querySelectorAll("ytd-video-renderer");
    log(`initCheckTrendingElements -> found ${trendingElements.length} elements`);

    for(let trendingElement of trendingElements) {
        consumeTrendingElement(trendingElement);
    }

    observerTrending.observe(parent, configTrending);
}


function findTrendingElements(node, findTrendingElementsCounter) {
    if (findTrendingElementsCounter < 0) {
        trackError(message="findTrendingElements-counter-0");
        return;
    }

    let queryResult = node.querySelectorAll('[id="contents"]');
    if (queryResult.length > 0) {
        initCheckTrendingElements(parent=queryResult[0]);
        return;

    } else {
        log('findTrendingElements -> findTrendingElementsCounter', findTrendingElementsCounter);
        setTimeout(findTrendingElements, 100, node, findTrendingElementsCounter - 1);
        return;
    }
}

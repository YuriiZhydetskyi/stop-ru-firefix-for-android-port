// Other tabs, we have to check content when we change tab, as new data is rendered in old elements.

const ytkConfigHomeSuggestion = { attributeFilter: [ "src", ], subtree: true }

const ytkCallbackHomeSuggestion = function(mutationsList, observer) {
// when we open another tab, Home videos are updated 
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let element = mutation.target.closest('ytk-compact-video-renderer');
                consumeElement(element);
            } catch(e) {
                log('Error in ytkCallbackHomeSuggestion', e);
            }
        }
    }
};

const ytkObserverHomeSuggestion = new MutationObserver(ytkCallbackHomeSuggestion);




function ytkInitCheckHomeSuggestion(parent) {
    let homeSuggestions = parent.querySelectorAll("ytk-compact-video-renderer");
    log(`initCheckHomeSuggestion -> found ${homeSuggestions.length} homeSuggestions`);
    
    for(let element of homeSuggestions) {
        consumeElement(element);
    }

    ytkObserverHomeSuggestion.observe(parent, ytkConfigHomeSuggestion);
}


function ytkHomeSuggestions(node, findHomeSuggestionsCounter) {

    if (findHomeSuggestionsCounter < 0) {
        trackError(message="ytkHomeSuggestions-counter-0", info={});
        return;
    }

    if (node.querySelectorAll('ytk-compact-video-renderer').length > 0) {
        log('findHomeSuggestions -> counter', findHomeSuggestionsCounter);
        stepFindElementHome = getStepLog(stepName='stepFindElementHome', stepCounter=findHomeSuggestionsCounter);
        ytkInitCheckHomeSuggestion(parent=node);
        return;

    } else {
        log('findHomeSuggestions -> findHomeSuggestionsCounter', findHomeSuggestionsCounter);
        let delay = 4000;
        if (findHomeSuggestionsCounter < 10) {
            delay = delay * 5;
        }
        setTimeout(ytkHomeSuggestions, delay, node, findHomeSuggestionsCounter - 1);
        return;
    }
}

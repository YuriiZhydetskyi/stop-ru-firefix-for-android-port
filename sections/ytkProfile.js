// Other tabs, we have to check content when we change tab, as new data is rendered in old elements.

const ytkConfigProfileSuggestion = { attributeFilter: [ "src", ], subtree: true }

const ytkCallbackProfileSuggestion = function(mutationsList, observer) {
// when we open another tab, Profile videos are updated 
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let element = mutation.target.closest('ytk-compact-video-renderer');
                consumeElement(element);
            } catch(e) {
                log('Error in ytkCallbackProfileSuggestion', e);
            }
        }
    }
};

const ytkObserverProfileSuggestion = new MutationObserver(ytkCallbackProfileSuggestion);




function ytkInitCheckProfileSuggestion(parent) {
    let ProfileSuggestions = parent.querySelectorAll("ytk-compact-video-renderer");
    log(`initCheckProfileSuggestion -> found ${ProfileSuggestions.length} ProfileSuggestions`);
    
    for(let element of ProfileSuggestions) {
        consumeElement(element);
    }

    ytkObserverProfileSuggestion.observe(parent, ytkConfigProfileSuggestion);
}


function ytkFindProfileSuggestions(node, findProfileSuggestionsCounter) {

    if (findProfileSuggestionsCounter < 0) {
        trackError(message="ytkProfileSuggestions-counter-0", info={});
        return;
    }

    if (node.querySelectorAll('ytk-compact-video-renderer').length > 0) {
        log('findProfileSuggestions -> counter', findProfileSuggestionsCounter);
        stepFindElementProfile = getStepLog(stepName='stepFindElementProfile', stepCounter=findProfileSuggestionsCounter);
        ytkInitCheckProfileSuggestion(parent=node);
        return;

    } else {
        log('findProfileSuggestions -> findProfileSuggestionsCounter', findProfileSuggestionsCounter);
        let delay = 4000;
        if (findProfileSuggestionsCounter < 10) {
            delay = delay * 5;
        }
        setTimeout(ytkFindProfileSuggestions, delay, node, findProfileSuggestionsCounter - 1);
        return;
    }
}

// Other tabs, we have to check content when we change tab, as new data is rendered in old elements.

const ytkConfigSearchSuggestion = { attributeFilter: [ "src", ], subtree: true }

const ytkCallbackSearchSuggestion = function(mutationsList, observer) {
// when we open another tab, Search videos are updated 
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let element = mutation.target.closest('ytk-compact-video-renderer');
                consumeElement(element);
            } catch(e) {
                log('Error in ytkCallbackSearchSuggestion', e);
            }
        }
    }
};

const ytkObserverSearchSuggestion = new MutationObserver(ytkCallbackSearchSuggestion);




function ytkInitCheckSearchSuggestion(parent) {
    let SearchSuggestions = parent.querySelectorAll("ytk-compact-video-renderer");
    log(`initCheckSearchSuggestion -> found ${SearchSuggestions.length} SearchSuggestions`);
    
    for(let element of SearchSuggestions) {
        consumeElement(element);
    }

    ytkObserverSearchSuggestion.observe(parent, ytkConfigSearchSuggestion);
}


function ytkFindSearchSuggestions(node, findSearchSuggestionsCounter) {

    if (findSearchSuggestionsCounter < 0) {
        trackError(message="ytkSearchSuggestions-counter-0", info={});
        return;
    }

    if (node.querySelectorAll('ytk-compact-video-renderer').length > 0) {
        log('findSearchSuggestions -> counter', findSearchSuggestionsCounter);
        stepFindElementSearch = getStepLog(stepName='stepFindElementSearch', stepCounter=findSearchSuggestionsCounter);
        ytkInitCheckSearchSuggestion(parent=node);
        return;

    } else {
        log('findSearchSuggestions -> findSearchSuggestionsCounter', findSearchSuggestionsCounter);
        let delay = 4000;
        if (findSearchSuggestionsCounter < 10) {
            delay = delay * 5;
        }
        setTimeout(ytkFindSearchSuggestions, delay, node, findSearchSuggestionsCounter - 1);
        return;
    }
}

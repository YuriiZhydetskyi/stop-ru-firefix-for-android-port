const ytkConfigSideSuggestion = { attributeFilter: [ "src", ], subtree: true }

const ytkCallbackSideSuggestion = function(mutationsList, observer) {
// when we open another video, side suggestions are updated 
    for(const mutation of mutationsList) {
        if (mutation.target.getAttribute('src') != null) {
            try {
                let element = mutation.target.closest('ytk-compact-video-renderer');
                consumeElement(element);
            } catch(e) {
                log('Error in ytkCallbackSideSuggestion', e);
            }
        }
    }
};

const ytkObserverSideSuggestion = new MutationObserver(ytkCallbackSideSuggestion);


function ytkInitCheckSideSuggestion(parent) {
    let sideSuggestions = parent.querySelectorAll("ytk-compact-video-renderer");
    log(`initCheckSideSuggestion -> found ${sideSuggestions.length} sideSuggestions`);

    for(let element of sideSuggestions) {
        consumeElement(element);
    }

    ytkObserverSideSuggestion.observe(parent, ytkConfigSideSuggestion);
}


function ytkFindSideSuggestions(node, findSideSuggestionsCounter) {
    if (!document.body.classList.contains('stopru-suggestions')) {
        log('findSideSuggestions - disabled');
        return;
    }

    if (findSideSuggestionsCounter < 0) {
        trackError(message="ytkFindSideSuggestions-counter-0", info={});
        return;
    }

    let queryResult = node.querySelectorAll('ytk-two-column-watch-next-results-renderer div[id=related]');
    if (queryResult.length > 0) {
        log('findSideSuggestions -> counter', findSideSuggestionsCounter);
        stepFindElementSideSuggestion = getStepLog(stepName='stepFindElementSide', stepCounter=findSideSuggestionsCounter);
        ytkInitCheckSideSuggestion(parent=queryResult[0]);
        return;

    } else {
        log('findSideSuggestions -> findSideSuggestionsCounter', findSideSuggestionsCounter);
        let delay = 4000;
        if (findSideSuggestionsCounter < 10) {
            delay = delay * 5;
        }
        setTimeout(ytkFindSideSuggestions, delay, node, findSideSuggestionsCounter - 1);
        return;
    }
}

chrome.runtime.onMessage.addListener(function(msg, sender){
    if(msg == "toggle-popup"){
        toggle();
    }
})

chrome.runtime.onMessage.addListener(function(msg, sender){
    if(msg == "close-popup"){
        let iframe = document.getElementById('stopRU');
        close(iframe);
    }
})

function close(iframe){
    if (iframe) {
        iframe.remove();
    }
}

function toggle(){
    let iframe = document.getElementById('stopRU');
    if (iframe) {
        close(iframe);
    } else {
        iframe = document.createElement('iframe'); 
        iframe.setAttribute('id', 'stopRU');
        iframe.style.height = "calc(100% - 14px)";
        iframe.style.maxHeight = "calc(100% - 14px)";
        iframe.style.width = "470px";
        iframe.style.position = "fixed";
        iframe.style.top = "7px";
        iframe.style.right = "0px";
        iframe.style.zIndex = "9000000000000000000";
        iframe.style.border = "0px"; 
        iframe.src = chrome.runtime.getURL("popup.html")

        document.body.appendChild(iframe);

        // add an event to close popup, only when popup is being open
        document.addEventListener('click', function () {
            if (document.getElementById('stopRU')) {
                close(document.getElementById('stopRU'));
            }
        });
    }
}

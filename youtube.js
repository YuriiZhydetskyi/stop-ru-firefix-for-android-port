const FIND_BUTTON_COUNTER = 5;
const CLICK_ON_BUTTON_COUNTER = 5;
const ACITON_VIDEO_HIGHLIGHT = 'highlight-video';

let elementsToRemove = new Set();
let stepFindPageManger;
let stepFindElementHome;
let stepFindElementSideSuggestion;
let currentAction;
let timerId = null;
let whitelistChannels = undefined;
let ytkMode = false;

log('Script STARTED');

if (getCookie('stopru-mixpanel-tracked') != 'true') {
    trackMixpanelEvent(
        message='',
        info=null,
        stepLogs=null,
        eventName='UserTrackDaily',
    );

    let exdexpireDate = new Date();
    exdexpireDate.setTime(exdexpireDate.getTime() + (1 * 24 * 60 * 60 * 1000));
    exdexpireDate.setHours(0, 0, 0, 0);
    setCookie('stopru-mixpanel-tracked', 'true', exdexpireDate);
}


function getStepLog(stepName, stepCounter) {
    return `${getDateTime(withDay=true)}-${twoDigits(stepCounter)}-${stepName}`;
}

function logObject(obj) {
    if(LOG_ENABLED == true && obj) {
        for (const [key, value] of Object.entries(obj)) {
            log(`  ${key}: ${value}`);
        }
    }
}

function enableSettingsUI(settingsToUpdate) {
    for (const [settingName, settingValue] of Object.entries(settingsToUpdate)) {
        switch (settingName) {
            case 'enableAnimation':
                if (settingValue == 'a-disabled') {
                    document.body.classList.remove("stopru-animation-enabled");
                } else {
                    document.body.classList.add("stopru-animation-enabled");
                }

                break;
            case 'enableSearchPage':
                if (settingValue == 'sp-disabled') {
                    document.body.classList.remove("stopru-page-search");
                } else {
                    document.body.classList.add("stopru-page-search");
                }

                break;
            case 'enableSuggestionsCheck':
                if (settingValue == 'vp-suggestions-disabled') {
                    document.body.classList.remove("stopru-suggestions");
                } else {
                    document.body.classList.add("stopru-suggestions");
                }

                break;
            case 'enableCommentsCheck':
                if (settingValue == 'vp-comments-disabled') {
                    document.body.classList.remove("stopru-comments");
                } else {
                    document.body.classList.add("stopru-comments");
                }

                break;
            default:
                break;
        }
    }
}

chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {
    enableSettingsUI(
        {
            'enableAnimation': response.data.enableAnimation,
            'enableSearchPage': response.data.enableSearchPage,
            'enableSuggestionsCheck': response.data.enableSuggestionsCheck,
            'enableCommentsCheck': response.data.enableCommentsCheck,
        }
    )
});

chrome.runtime.onMessage.addListener((msg, sender, resp) => {
    if (msg.command == "enable-settings-ui") {
        enableSettingsUI(msg.data);
        resp({'status': 'OK'});
    }
});

let elementsToCheck = [];

function convertDurationToSeconds(durationText) {
    let durationArray = durationText.split(':');

    if (durationArray.length < 2) {
        return 0;
    }

    if (durationArray.length == 2) {
        durationArray.unshift('00');
    }

    durationArray = durationArray.map(Number);

    return durationArray[0] * 3600 + durationArray[1] * 60 + durationArray[2];
}

function trackInfo(message, info, stepLogs) {
    trackMixpanelEvent(message, info, stepLogs, eventName='infoEvent');
}

function trackError(message, info, stepLogs) {
    trackMixpanelEvent(message, info, stepLogs, eventName='errorEvent');
}

function trackMixpanelEvent(message, info, stepLogs, eventName) {
    log('trackMixpanelEvent', eventName, message, info);
    logObject(stepLogs);

    let properties = {
        message: message,
        ...getEventMetaData(),
        ...info,
    };

    if (stepLogs) {
        properties['stepLogs'] = stepLogs;
    }

    chrome.runtime.sendMessage({
        command: 'mixnel-track',
        data: {
            eventName: eventName,
            properties: properties,
        },
    }, (response) => {
        log('trackMixpanelEvent -> response', response);
    });
}

function isInWhitelist(channelId) {
    var postData = {
        channelId: channelId,
    }
    chrome.runtime.sendMessage({command: 'is-in-check-whitelist', data: postData}, (response) => {
        log('is-in-check-whitelist:', response);
        return response.isInWhitelist;
    });
}

function checkText(text) {
    // true - remove
    // false - keep

    function isRU(text) {
        return (
            text.includes('ё') ||
            text.includes('ъ') ||
            text.includes('ы') ||
            text.includes('э')
        );
    }

    function isUA(text) {
        return (
            text.includes('ґ') ||
            text.includes('є') ||
            text.includes('і') ||
            text.includes('ї') ||
            text.includes('ў')  // Belarusian alphabet
        );
    }

    return isRU(text.toLowerCase()) && !isUA(text.toLowerCase());
}

function getMenuActionItemNumber(popup, actionName) {
    // I want to keep Text and Icon comparation to make code more resiliant to YouTube UI changes,
    // as in case of any Text or Icon changes program will still work for most users (with languages UA, EN, RU)

    let notInterestedIcon = "M18.71 6C20.13 7.59 21 9.69 21 12c0 4.97-4.03 9-9 9-2.31 0-4.41-.87-6-2.29L18.71 6zM3 12c0-4.97 4.03-9 9-9 2.31 0 4.41.87 6 2.29L5.29 18C3.87 16.41 3 14.31 3 12zm9-10c5.52 0 10 4.48 10 10s-4.48 10-10 10S2 17.52 2 12 6.48 2 12 2z";
    let dontRecommendChannelIcon = "M12,3c-4.96,0-9,4.04-9,9s4.04,9,9,9s9-4.04,9-9S16.96,3,12,3 M12,2c5.52,0,10,4.48,10,10s-4.48,10-10,10S2,17.52,2,12 S6.48,2,12,2L12,2z M19,13H5v-2h14V13z";
    let menuItemElementList = getMenuItemElementList(popup);

    if (actionName == 'highlight-video') {
        return {menuItemNumber: -1, actualActionName: actionName};
    }

    if (ytkMode) {
        if (menuItemElementList.length == 1) {
            return {menuItemNumber: 0, actualActionName: actionName};
        } else {
            return {menuItemNumber: -1, actualActionName: actionName};
        }
    }

    for(let [i, menuItemElement] of Object.entries(menuItemElementList)) {
        let itemName = menuItemElement.querySelectorAll("yt-formatted-string")[0].innerText;
        let pathElement = menuItemElement.querySelector("path");
        let itemIcon = pathElement  && pathElement.getAttribute("d");

        switch (actionName) {
            case 'cancel-video':
                if (["Не цікавить", "Not interested", "Не интересует"].includes(itemName) == true) {
                    return {menuItemNumber: i, actualActionName: actionName};
                }
                if (itemIcon == notInterestedIcon) {
                    return {menuItemNumber: i, actualActionName: actionName};
                }
                break;

            case 'cancel-channel':
                if (["Не рекомендувати канал", "Don't recommend channel", "Не рекомендовать видео с этого канала"].includes(itemName) == true) {
                    return {menuItemNumber: i, actualActionName: actionName};
                }
                if (itemIcon == dontRecommendChannelIcon) {
                    return {menuItemNumber: i, actualActionName: actionName};
                }
                break;

            case 'highlight-video':
                break;

            default:
                break;
        }
    }

    if (actionName == 'cancel-channel') {
        // if there is no option to remove a channel we have to try to cancel video
        return getMenuActionItemNumber(popup, 'cancel-video');
    }
    trackError(message='No menu element for action', info={"action": actionName, "menuItems": getMenuItemsFromPopup(popup)});
    // add menu items into a track, where an element comes from
    return {menuItemNumber: -1, actualActionName: actionName};
}

function getAvailablePopups() {
    if (ytkMode) {
        return document.querySelectorAll("ytk-popup-container tp-yt-iron-dropdown ytk-menu-popup-renderer");    
    }
    return document.querySelectorAll("ytd-popup-container tp-yt-iron-dropdown ytd-menu-popup-renderer");
};


function getMenuItemsFromPopup(popup) {
    return Array.from(popup.querySelectorAll("tp-yt-paper-item")).map(item => item.innerText.trim());
}

function getAvailablePopupsWithMenuItems() {
    let availablePopups = {};
    for(let [i, p] of Object.entries(getAvailablePopups())) {
        let mList = getMenuItemsFromPopup(p);
        availablePopups[i] = `${mList}`;
    }
    return availablePopups;
};

function cancelButtonHandler(element,  stepLogs, cancelButtonHandlerCounter) {
    let selectorResult = element.querySelectorAll("[id=dismissed] ytd-notification-multi-action-renderer button");
    if (selectorResult.length == 2 || selectorResult.length == 1) {
        let cancelButton = selectorResult[0];
        cancelButton.addEventListener('click', function () {
            stepLogs['stepCancelRemoval'] = getStepLog(stepName='stepCancelRemoval', stepCounter=cancelButtonHandlerCounter);

            trackInfo(
                message="Cancel removal",
                info={
                    "cancelButton": cancelButton.innerText,
                    ...getElementData(element),
                },
                stepLogs=stepLogs,
            );
        },
        {
            once: true,
        });
    } else {
        if (cancelButtonHandlerCounter <= 0) {
            trackError(
                message='No cancel button',
                info={
                    "cancelButtons": Array.from(selectorResult).map(btn => btn.innerText.trim()),
                    "textOfElement": element.innerText,
                    ...getElementData(element),
                },
            );
            return -1;
        }

        setTimeout(cancelButtonHandler, 50, element, stepLogs, cancelButtonHandlerCounter - 1);
    }
}

function getMenuItemElementList(popup) {
    if (ytkMode) {
        return popup.querySelectorAll("ytk-menu-service-item-renderer");
    }
    
    return popup.querySelectorAll("ytd-menu-service-item-renderer");
}

function clickOnMenu(element, popup, stepLogs, actionName, menuClickCounter) {
try {
    if (menuClickCounter < 0) {
        trackError(
            message='No elements in menu.',
            info={
                "popup": popup.innerHTML.slice(700, 1500),
                "menuItems": getMenuItemsFromPopup(popup),
            },
            stepLogs=stepLogs,
        );
        return;
    }

    let menuItemElementList = getMenuItemElementList(popup);
    if (menuItemElementList.length == 0) {
        setTimeout(clickOnMenu, 50, element, popup, stepLogs, actionName, menuClickCounter - 1);
        return;
    }

    let {menuItemNumber, actualActionName} = getMenuActionItemNumber(popup, actionName);
    
    if (menuItemNumber < 0) {
        return;
    }

    if (!element.classList.contains('stopru-video')) {
        trackInfo(
            message="Outdated video",
            info={
                ...getElementData(element),
            },
            stepLogs=stepLogs,
        );
        return;
    }

    if (checkElement(element) == false) {
        trackError(
            message="Is not Ru video #1",
            info={
                ...getElementData(element),
            },
            stepLogs=stepLogs,
        );
        return;
    }

    if (ytkMode) {
        menuItemElementList[menuItemNumber].querySelector('tp-yt-paper-item').click();
    } else {
        menuItemElementList[menuItemNumber].click();
    }
    
    element.classList.remove("stopru-video");

    stepLogs['stepMenuClick'] = getStepLog(stepName='stepMenuClick', stepCounter=menuClickCounter);

    if (!ytkMode) {
        // no cancel button in ytk
        setTimeout(cancelButtonHandler, 10, element, stepLogs, 5);
    }

    let eventName = 'notSetEventName';
    let commandName;

    if (actualActionName == 'cancel-video') {  // not interested
        eventName = 'videoRemoved';
        commandName = 'create-video-blocked';
    }

    if (actualActionName == 'cancel-channel') {  // don't recommend channel
        eventName = 'channelRemoved';
        commandName = 'create-channel-blocked';
    }

    let postData = getElementData(element);
    if (commandName) {
        chrome.runtime.sendMessage({command: commandName, data: postData}, (response) => {
            log('create item in DB -> response', response);
        });
    }

    logObject(stepLogs);
    chrome.runtime.sendMessage({
        command: 'mixnel-track',
        data: {
            eventName: eventName,
            properties: {...postData, ...getEventMetaData(), stepLogs},
        },
    }, (response) => {
        log('mixnel-track -> response received', response);
    });

} catch(e) {
    trackError(
        message="clickOnMenu exception",
        info={
            ...getElementData(element),
            error: e.stack,
        },
        stepLogs=stepLogs,
    );
    setTimeout(clickOnMenu, 50, element, popup, stepLogs, actionName, menuClickCounter - 1);
    return;
}
}

function choosePopup(element, button, actionName, stepLogs, choosePopupCounter) {
    button.click();
    document.body.classList.remove("stopru-on-click");

    // choose a proper popup
    let popups = getAvailablePopups();

    if (popups.length == 0) {
        if (choosePopupCounter < 0) {
            trackError(message='No popup', info={...getElementData(element), 'availablePopups': getAvailablePopupsWithMenuItems()}, stepLogs=stepLogs);
            return;
        }

        setTimeout(clickOnButton, 10, element, button, actionName, stepLogs, choosePopupCounter - 1);
        return;
    } else if(popups.length > 1) {
        trackError(message='Multiple popups', info={...getElementData(element), 'availablePopups': getAvailablePopupsWithMenuItems()}, stepLogs=stepLogs);
        return;
    }

    let popup = popups[0];

    stepLogs['stepFindPopup'] = getStepLog(stepName='stepFindPopup', stepCounter=choosePopupCounter);
    // 0 -> 10, to give a time to gender menu elements, d
    setTimeout(clickOnMenu, 10, element, popup, stepLogs, actionName, 8);
    log('choosePopup -> choosePopupCounter', choosePopupCounter);
}

function clickOnButton(element, button, actionName, stepLogs, clickOnButtonCounter) {
    stepLogs['stepClickOnButton'] = getStepLog(stepName='stepClickOnButton', stepCounter=clickOnButtonCounter);
    document.body.classList.add("stopru-on-click");
    button.click();
    setTimeout(choosePopup, 0, element, button, actionName, stepLogs, clickOnButtonCounter);  // 10 * 5 ms
}

function getVideoName(element) {
    let selectorResult;
    
    if (ytkMode) {
        selectorResult = element.querySelectorAll('.primary-text.style-scope.ytk-compact-video-renderer');
    } else {
        selectorResult = element.querySelectorAll('[id="video-title"]');
    }
    
    if (selectorResult.length > 0) {
        return selectorResult[0].innerText.trim();
    }

    return '';
}

function getVideoNameOld(element) {
    return element.getAttribute("sr-name");
}

function getChannelName(element) {
    selectorResult = element.querySelectorAll('#channel-name yt-formatted-string.ytd-channel-name');
    if (selectorResult.length > 0) {
        return selectorResult[0].innerText.trim();
    }

    return '';
}

function getVideoDuration(element) {
    let videoDuration = '';
    let selectorResult = element.querySelectorAll('span.ytd-thumbnail-overlay-time-status-renderer');

    if (selectorResult.length > 0) {
        videoDuration = selectorResult[0].innerText;
    }
    return videoDuration && videoDuration.trim();
}

function getVideoId(element) {
    // videoId = (href && href.split('/shorts/')[1]);  // only shorts on main page
    // videoId = (href && href.split('?v=')[1]);  // all other cases: regular video on home page & sideSuggestion
    let hrefElement;
    
    if (ytkMode) {
        hrefElement = element.querySelector('a');
    } else {
        hrefElement = element.querySelector('[id="video-title-link"]') || element.querySelector('[id="thumbnail"]');
    }

    if (!hrefElement) {
        return '';
    }

    let href = hrefElement.getAttribute('href');
    if (!href) {
        return '';
    }
    
    return (href.split('?v=')[1] || href.split('/shorts/')[1] || '').trim();
}

function getVideoData(element) {
    return {
        'videoName': getVideoName(element),
        'videoId': getVideoId(element),
    }
}

function getChannelData(element) {
    let channelId = '';

    selectorResult = element.querySelectorAll('[id="channel-name"]');
    if (selectorResult.length > 0) {

        selectorResult = selectorResult[0].getElementsByClassName('yt-simple-endpoint');
        if (selectorResult.length > 0) {
        channelId = selectorResult[0].getAttribute('href');
        }
    }

    return {
        'channelName': getChannelName(element),
        'channelId': channelId && channelId.trim(),
    }
}

function getElementData(element) {
    let videoData = getVideoData(element);
    let channelData = getChannelData(element);
    let videoDuration = getVideoDuration(element);

    return {
        elementName: getElementName(element),
        channelId: channelData['channelId'],
        channelName: channelData['channelName'],
        videoId: videoData['videoId'],
        videoName: videoData['videoName'],
        videoNameOld: getVideoNameOld(element),
        videoDuration: videoDuration,
        videoDurationSeconds: convertDurationToSeconds(videoDuration),
    }
}

function getAvailableButtonsWithDescription (element) {
    availableButtons = {};
    for(let [i, b] of Object.entries(element.querySelectorAll('button'))) {
        availableButtons[i] = `${b.getAttribute('id')} - ${b.innerText} - ${b.classList}`;
    }
    return availableButtons;
}

function isVideoInFullscreen() {
    return document.fullscreenElement != null;
}

function isInViewport(element) {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function isInIncognitoMode() {
    if (ytkMode) {
        // we assume that in ytk there 4 elements in menu in incognito mode
        return document.querySelectorAll("#anchors-row-content ytk-kids-category-tab-renderer").length == 4;
    }
    return (document.getElementById('avatar-btn') === null);  // Incognito
}

function getElementName(element) {
    return element.tagName;
}

function checkElement(element, extraText) {
    // returns true if we have to remove the video

    let videoName = getVideoName(element);
    let channelName = getChannelName(element);
    extraText = (extraText === undefined) ? "" : extraText;

    if (whitelistChannels.includes(channelName.toLowerCase()) == true) {
        element.classList.add("stopru-whitelisted");  // remove in consumeElement
        return false;
    }

    if (checkText(videoName + channelName + extraText)) {
        return true;
    }

    return false;
}

function processElementList() {
    if (elementsToCheck.length == 0) {
        return;
    }

    log('processElementList', elementsToCheck.length);
    let elements = elementsToCheck;
    elementsToCheck = [];
    
    let videosRuCounter = 0;
    for(let element of elements) {
        let isReconsumed = false;
        if (elementsToRemove.has(element) == true) {
            isReconsumed = true;
            // Don't treat it as an error. It's more to understand how often it could happen
            // 340 cases on 1938 removed videos (video + channel)
            // trackInfo(
            //     message="Consumed element in queue",
            //     info={
            //         ...getElementData(element),
            //     },
            // );
        }

        if (checkElement(element) == true) {
            log('Element', getVideoName(element), 'is RU');
            if (isReconsumed == true) {
                continue;
            }

            element.classList.add("stopru-video");
            videosRuCounter += 1;

            if (isInIncognitoMode() == true) {
                continue;
            }
            if (currentAction == ACITON_VIDEO_HIGHLIGHT) {
                continue;
            }

            element.setAttribute('removeCounter', FIND_BUTTON_COUNTER);  // at least FIND_BUTTON_COUNTER x 2s to remove
            elementsToRemove.add(element);
            if (timerId == null) {
                timerId = setInterval(removeElementHandler, 2000);
            }
        } else {
            elementsToRemove.delete(element);
            element.classList.remove("stopru-video");
        }
    }
    
    var postData = {
        videosTotal: elements.length,
        videosRu: videosRuCounter,
    }

    chrome.runtime.sendMessage({command: 'mixnel-increment', data: postData}, (response) => {
        log('mixnel-increment -> response', response);
    });
}

function consumeElement(element) {
    log('consumeElement', elementsToCheck.length);

    if (element == null) {
        return;
    }

    let videoNameOld = getVideoNameOld(element);
    if (videoNameOld && videoNameOld == getVideoName(element)) {  // don't process element, if the name is the same
        return;
    }

    element.classList.remove("stopru-video");
    element.classList.remove("stopru-whitelisted");
    element.removeAttribute("removeCounter");
    element.setAttribute("sr-name", getVideoName(element));
    elementsToCheck.push(element);

    if (elementsToCheck.length == 1) {
        setTimeout(processElementList, 3000);
    }

    if (elementsToCheck.length >= 24) {
        // 24 is an amount that youtube load per one page (double check it) 
        processElementList();
    }
}

const config = { attributeFilter: [ "src", ], subtree: true }  // attributeOldValue: true, mutation.oldValue;

const callback = function(mutationsList, observer) {
    // The callback is called twice per video element update:
    //  1/ when channel image src attribute is set
    //  2/ when video image src attribute is set
    // To redraw a video element by YouTube (when user press YouTube button) an element is redrawn in two steps:
    //  1/ channel image src attribute is set null & video image src attribute is set null
    //  2/ channel image src attribute is set & video image src attribute is set
    // That's why we track video name in element attribute 'sr-name' and track video image src update only.

    for(const mutation of mutationsList) {
        if (
            (mutation.target.parentElement && mutation.target.parentElement.parentElement && mutation.target.parentElement.parentElement.getAttribute('id') == 'thumbnail') 
            && (mutation.target.getAttribute('src') != null)
        ) {
            let element = mutation.target.closest('ytd-rich-item-renderer');
            consumeElement(element);
        }
    }
};

const observer = new MutationObserver(callback);


function initCheckHome(parent) {
    var elements = parent.querySelectorAll("ytd-rich-item-renderer");

    log(`initCheckHome -> found ${elements.length} elements`);
    
    for(let element of elements) {
        try {
            let image = element.querySelector("[id='thumbnail'] img");
            if (image && image.getAttribute('src')) {
                consumeElement(element);
            }
        } catch(error) {
            trackError(message="initCheckHomeError", info={"error": error.stack.toString()});
        }
        // log('initCheckHome -> ', getVideoName(element));
        // element.style.border = 'solid red';
    }

    const targetNode = parent;
    observer.observe(targetNode, config);
}


function findElementsHome(parent, findElementsHomeCounter) {
    if (findElementsHomeCounter && findElementsHomeCounter <= 0) {
        trackError(message="findElementsHome-counter-0");
        return;
    }

    if (parent.querySelectorAll('[id="contents"]').length > 0) {
        stepFindElementHome = getStepLog(stepName='stepFindElementHome', stepCounter=findElementsHomeCounter);
        initCheckHome(parent=parent.querySelectorAll('[id="contents"]')[0]);
        return;

    } else {
        log('findElementsHome -> findElementsHomeCounter', findElementsHomeCounter);
        setTimeout(findElementsHome, 100, parent, findElementsHomeCounter - 1);
        return;
    }
}


function processPageManagerChild(node) {
    switch(node.tagName.toLowerCase()) {
        case 'ytd-search':
            log('we process search page');
            setTimeout(findElementsSearch, 100, node, 20);
            break;
        case 'ytd-browse':
            switch(node.getAttribute('page-subtype')) {
                case 'home':
                    log('we process home page');
                    setTimeout(findElementsHome, 100, node, 40);
                    break;
                case 'trending':
                    log('we process trending page');
                    setTimeout(findTrendingElements, 100, node, 40);
                    break;
                case 'subscriptions':
                    log('we process subscriptions page');
                    break;
                default:
                    break;
                    // log(`not suportned ${node.getAttribute('page-subtype')} page`);
            }
            break;
        case 'ytd-watch-flexy':
            setTimeout(findComments, 2000, node, 20);  // 20 times, every 2 second => 40seconds
            setTimeout(findSideSuggestions, 4000, node, 20);  // 20 times, every 6 second => 120seconds
            break;
        case 'ytd-shorts':
            break;
        // ytk
        case 'div': // loading
            log('loading');
            break;
        case 'ytk-kids-home-screen-renderer':  // main page
            log('ytk-kids-home-screen-renderer');
            setTimeout(ytkHomeSuggestions, 4000, node, 20);  // 20 times, every 6 second => 120seconds
            break;
        case 'ytk-watch-page':  // watch a video
            log('ytk-watch-page');
            setTimeout(ytkFindSideSuggestions, 4000, node, 20);  // 20 times, every 6 second => 120seconds
            break;
        case 'ytk-browse-response':  // idk
            log('ytk-browse-response');
            break;
        case 'ytk-search-response':  // search page
            log('ytk-search-response');
            setTimeout(ytkFindSearchSuggestions, 4000, node, 20);  // 20 times, every 6 second => 120seconds
            break;
        case 'ytk-wia-page':  // own profile page
            setTimeout(ytkFindProfileSuggestions, 4000, node, 20);  // 20 times, every 6 second => 120seconds
            break;
        case 'ytk-settings-page':
            break;
        case 'ytk-kids-onboarding-flow-data-v2':
                break;
        case 'ytk-parent-profile-settings-page':
            break;

        default:
            trackError(message="Unknown page", info={"tagName": node.tagName.toLowerCase()});
    }
}

const configPageManagerElement = { childList: true, subtree: false }

const callbackPageManagerElement = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        for(let [i, node] of Object.entries(mutation.addedNodes)) {
            processPageManagerChild(node);
        }
    }
};

const observerPageManagerElement = new MutationObserver(callbackPageManagerElement);

function processPageManagerElement() {
    let targetNode;
    if (ytkMode) {
        targetNode = document.getElementById('page-root');
    } else {
        targetNode = document.getElementById('page-manager');
    }

    // in case it's already rendered
    for (const node of targetNode.children) {
        processPageManagerChild(node);
    }

    // in case it will be rendered a bit later
    observerPageManagerElement.observe(targetNode, configPageManagerElement);
}

function findPageManagerElement(findPageManagerCounter) {
    if (findPageManagerCounter < 0) {
        if (window.location.href.startsWith('https://www.youtube.com') == false) {
            return;
        }

        if (window.location.href.startsWith('https://www.youtube.com/supported_browsers') == true) {
            return;
        }

        if (window.location.href.startsWith('https://www.youtube.com/verify_phone_number') == true) {
            return;
        }

        trackError(message="findPageManager-counter-0");
        return;
    }

    let rootElementNotAvailable = true;
    if (ytkMode) {
        rootElementNotAvailable = document.getElementById('page-root') == null;
    } else {
        rootElementNotAvailable = document.getElementById('page-manager') == null;
    }

    if (rootElementNotAvailable) {
        log('findPageManagerElement -> findPageManagerCounter', findPageManagerCounter);
        let delay = 100;
        if (findPageManagerCounter < 10) {
            delay = delay * 4;
        }
        setTimeout(findPageManagerElement, delay, findPageManagerCounter - 1);
        return;
    } else {
        stepFindPageManger = getStepLog(stepName='stepFindPageManger', stepCounter=findPageManagerCounter);
        processPageManagerElement();
        return;
    }
}

function getInitStepLogsData(element) {
    let stepLogs = {
        stepFindPageManger,
        // 'stepFindElementIsRu': getStepLog(
        //     stepName=`stepFindElementIsRu->${getElementName(element)}->${getVideoName(element)}->${getChannelName(element)}`,  // !
        //     stepCounter=0,
        // ),
    };

    if (element.hasAttribute('sr-parent') == true) {  // doesn't work
        if (element.getAttribute('sr-parent') == 'home'){
            stepLogs['stepFindParentElement'] = stepFindElementHome;
        } else if (element.getAttribute('sr-parent') == 'side'){
            stepLogs['stepFindParentElement'] = stepFindElementSideSuggestion;
        } else {
            stepLogs['stepFindParentElement'] = getStepLog(
                stepName=`parentUnknown->${getElementName(element)}->${getVideoName(element)}->${getChannelName(element)}`,
                stepCounter=0,
            );
        }
    }
    return stepLogs;
}

function removeElementHandler() {
    log('removeElementHandler', elementsToRemove.size);
    try {
        if (elementsToRemove.size == 0) { clearInterval(timerId); timerId = null; }
        if (isVideoInFullscreen() == true) { return; }  // jump to the next iteration
        if (isInIncognitoMode() == true) { return; }

        for (let element of elementsToRemove) {
            log('processElement', getVideoName(element));
            if (checkElement(element) == false) {
                elementsToRemove.delete(element);
                element.classList.remove("stopru-video");
                trackInfo(
                    message="Is not Ru video #2",
                    info={
                        ...getElementData(element),
                    },
                );
                continue;
            }
            if (isInViewport(element) == false) { log("not is view port"); continue; }

            let buttons;
            if (ytkMode) {
                buttons = element.querySelectorAll('tp-yt-paper-icon-button[id="menu-button"]');
            } else {
                buttons = element.querySelectorAll('button[id="button"]');
            }
            let iteration = element.getAttribute('removeCounter') || FIND_BUTTON_COUNTER;
            if (buttons.length == 1) {
                log('found button');
                let stepLogs = getInitStepLogsData(element);
                stepLogs['stepFindButton'] = getStepLog(stepName='stepFindButton', stepCounter=iteration);
                setTimeout(clickOnButton, 0, element, buttons[0], currentAction, stepLogs, CLICK_ON_BUTTON_COUNTER);  // 10 * 5 ms
                elementsToRemove.delete(element);
                return;
            } else if (buttons.length == 0) {  // no button, try later, decrease counter attribute
                if (iteration > 0) {
                    element.setAttribute("removeCounter", iteration - 1);
                } else {
                    trackError(
                        message="No button",
                        info={
                            ...getElementData(element),  // move to the root
                            availableButtons: getAvailableButtonsWithDescription(element),
                        },
                    );
                    elementsToRemove.delete(element);
                }
                continue;
            } else {  // multiple buttons, break removal process, track error
                trackError(
                    message='Multiple buttons',
                    info={
                        ...getElementData(element),  // move to the root
                        availableButtons: getAvailableButtonsWithDescription(element),
                    },
                );
                elementsToRemove.delete(element);  // remove the class or keep it?
                continue;
            }
        }
    } catch(error) {
        trackError(message="RemoveElementError", info={"error": error.stack.toString()});
    }
}

if (window.location.href.startsWith('https://www.youtube.com') == true || window.location.href.startsWith('https://www.youtubekids.com') == true){
    if(window.location.href.startsWith('https://www.youtubekids.com') == true) {
        ytkMode = true;
    }
    findPageManagerElement(50);

    chrome.runtime.sendMessage({command: 'get-user-profile', data: {}}, (response) => {
        log('get-user-profile -> response', response);
        currentAction = response.data.action;
        whitelistChannels = (response.data['whitelist-channels'] || []).map(obj => obj.channelName && obj.channelName.toLowerCase());
    });
}
